/**
 * 
 */
package com.delta.cru.rest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.delta.cru.rest.mapper.EmpMapper;
import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */
@Component
public class EmpDaoImpl implements EmpDaoIfc {

	@Autowired
	private EmpMapper empMapper;

	@Override
	public EmpVo fEmpById(String employeeid) {
		return empMapper.findEmpById(employeeid);
	}

	@Override
	public List<EmplPhVo> fPhVos() {
		return empMapper.findAllEmpPhns();
	}

}
