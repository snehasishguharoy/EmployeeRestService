/**
 * 
 */
package com.delta.cru.rest.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.delta.cru.rest.dao.EmpDaoIfc;
import com.delta.cru.rest.exp.EmpException;
import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */
@Component
public class EmpDtlsImpl implements EmpDtlsIfc {
	private static final Logger log = LoggerFactory.getLogger(EmpDtlsImpl.class);

	@Autowired
	private EmpDaoIfc empDaoIfc;

	@Override
	public EmpVo findEmployeeById(String employeeid) throws EmpException {
		log.info("#### findAllEmployees");
		if (null == empDaoIfc.fEmpById(employeeid)) {
			throw new EmpException("The Employee Details are missing");
		}
		return empDaoIfc.fEmpById(employeeid);
	}

	@Override
	public List<EmplPhVo> findEmployeesPh() throws EmpException {
		log.info("#### findEmployeesPh");
		if (null == empDaoIfc.fPhVos()) {
			throw new EmpException("The Employee Phno details are missing");
		}
		return empDaoIfc.fPhVos();
	}

}
