/**
 * 
 */
package com.delta.cru.rest.dao;

import java.util.List;

import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */
public interface EmpDaoIfc {

	EmpVo fEmpById(String employeeid);

	List<EmplPhVo> fPhVos();

}
