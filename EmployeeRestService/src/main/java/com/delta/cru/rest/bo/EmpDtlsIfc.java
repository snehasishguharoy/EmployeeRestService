/**
 * 
 */
package com.delta.cru.rest.bo;

import java.util.List;

import com.delta.cru.rest.exp.EmpException;
import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */
public interface EmpDtlsIfc {

	EmpVo findEmployeeById(String employeeid) throws EmpException;

	List<EmplPhVo> findEmployeesPh() throws EmpException;

}
