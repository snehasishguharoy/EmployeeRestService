/**
 * 
 */
package com.delta.cru.rest.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */
public interface EmpMapper {

	@Select("SELECT  E.EMPL_ID,EMPL_DSPLY_NM FROM EMPLOYEE E where E.EMPL_ID=#{employeeid}")
	@Results({ @Result(id = true, column = "EMPL_ID", property = "emplId"),
			@Result(column = "EMPL_DSPLY_NM", property = "emplDisplayName") })

	EmpVo findEmpById(String employeeid);

	@Select("SELECT  EP.EMPL_ID,EP.PH_TYP_CD,EP.EMPL_PH_NB,EP.EFF_DT,EP.EXP_DT FROM EMPL_PH EP ORDER BY EP.EFF_DT DESC,EP.PH_TYP_CD,EP.EMPL_PH_NB")
	@Results({ @Result(id = true, column = "EMPL_ID", property = "emplId"),
			@Result(id = true, column = "PH_TYP_CD", property = "phTypCd"),
			@Result(id = true, column = "EMPL_PH_NB", property = "emplPhNb"),
			@Result(id = true, column = "EFF_DT", property = "effDt"), @Result(column = "EXP_DT", property = "expDt")

	})
	List<EmplPhVo> findAllEmpPhns();

}
