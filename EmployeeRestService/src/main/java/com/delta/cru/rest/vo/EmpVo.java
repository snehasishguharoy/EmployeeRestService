/**
 * 
 */
package com.delta.cru.rest.vo;

import java.io.Serializable;

/**
 *
 */
public class EmpVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8642735862039852046L;

	private String emplId;
	private String emplDisplayName;

	public String getEmplId() {
		return emplId;
	}

	public void setEmplId(String emplId) {
		this.emplId = emplId;
	}

	public String getEmplDisplayName() {
		return emplDisplayName;
	}

	public void setEmplDisplayName(String emplDisplayName) {
		this.emplDisplayName = emplDisplayName;
	}

	public EmpVo() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
