/**
 * 
 */
package com.delta.cru.rest.exp;

/**
 *
 */
public class EmpException extends Exception {

	public EmpException(String message) {
		super(message);
	}

}
