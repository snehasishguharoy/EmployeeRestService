/**
 * 
 */
package com.delta.cru.rest.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 *
 */
@SpringBootApplication(exclude = { MessageSourceAutoConfiguration.class })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
public class RestPocApplication extends SpringBootServletInitializer {
	private static final Class<RestPocApplication> applicationClass = RestPocApplication.class;
	// private static final Logger log =
	// LoggerFactory.getLogger(applicationClass);

	public static void main(String[] args) {
		SpringApplication.run(RestPocApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

}
