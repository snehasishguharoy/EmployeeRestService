/**
 * 
 */
package com.delta.cru.rest.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import com.delta.cru.rest.cons.GenericConstant;
import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */
@Configuration
@MappedTypes(value = { EmpVo.class, EmplPhVo.class })
@MapperScan("com.delta.cru.rest.mapper")

public class EmpRstCnfg {

	/** The client id. */

	@Bean
	public DataSource dataSource() {
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		dataSourceLookup.setResourceRef(true);
		String dataSourceName = GenericConstant.JNDI_NAME;
		return dataSourceLookup.getDataSource(dataSourceName);
	}

	@Bean
	public SqlSessionFactory sessionFactory() throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource());
		return bean.getObject();
	}

}
