/**
 * 
 */
package com.delta.cru.rest.svc;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.cru.rest.bo.EmpDtlsIfc;
import com.delta.cru.rest.exp.EmpException;
import com.delta.cru.rest.vo.EmpVo;
import com.delta.cru.rest.vo.EmplPhVo;

/**
 *
 */

@RestController
public class EmpSvc {
	private static final Logger LOGGER = Logger.getLogger(EmpSvc.class.getName());

	@Autowired
	private EmpDtlsIfc dtlsIfc;

	@RequestMapping(value = "/name/{employeeid}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<EmpVo> getAllEmpDetails(@PathVariable("employeeid") String employeeid)
			throws EmpException {
		LOGGER.info("Printing the particular employee detail");
		HttpStatus httpStatus = null != dtlsIfc.findEmployeeById(employeeid) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
		return new ResponseEntity<EmpVo>(dtlsIfc.findEmployeeById(employeeid), httpStatus);
	}

	@RequestMapping(value = "/phonenumbers", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<EmplPhVo>> getAllEmpPhns() throws EmpException {
		LOGGER.info("Printing the ordered by employee details");
		HttpStatus httpStatus = null != dtlsIfc.findEmployeesPh() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
		return new ResponseEntity<List<EmplPhVo>>(dtlsIfc.findEmployeesPh(), httpStatus);

	}

}
